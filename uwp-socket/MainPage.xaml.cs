﻿using System;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Controls;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x410

namespace uwp_socket {

    public sealed partial class MainPage : Page {


        public MainPage() {

            this.InitializeComponent();

            initSocketServer();
        }


        private StreamSocketListener listener; // the socket listner to listen for TCP requests
                                               // Note: this has to stay in scope!

        private const uint BufferSize = 8192; // this is the max size of the buffer in bytes 


        void initSocketServer() {
            listener = new StreamSocketListener();
            listener.ConnectionReceived += async (sender, args) => {
                var result = await ReadWithDataReader ( args.Socket );
                SendWithDataWriter( args.Socket, result );
            };
            listener.BindServiceNameAsync("8888").AsTask().Wait();
        }

        void initSocketClient() {
            StreamSocket socket = new StreamSocket();
            socket.ConnectAsync(new HostName("192.168.178.66"), "8888").AsTask().Wait();
            SendWithDataWriter(socket, "ciao ciccio");
        }



        #region WRITE

        /// <summary>
        /// Utilizza la classe DataWrite che semplifica notevolmente l'invio di oggetti complessi
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="message"></param>
        public async void SendWithDataWriter ( StreamSocket socket, string message ) {
             DataWriter writer;

            // Create the data writer object backed by the in-memory stream. 
            using (writer = new DataWriter(socket.OutputStream)) {
                // Set the Unicode character encoding for the output stream
                writer.UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding.Utf8;
                // Specify the byte order of a stream.
                writer.ByteOrder = Windows.Storage.Streams.ByteOrder.LittleEndian;

                // Gets the size of UTF-8 string.
                writer.MeasureString(message);
                // Write a string value to the output stream.
                writer.WriteString(message);

                // Send the contents of the writer to the backing stream.
                await writer.StoreAsync();

                await writer.FlushAsync();

                writer.DetachStream();
            }
        }
        
        // NON TESTATO
        public async void SendWithBuffer ( StreamSocket socket, string message ) {
            // Send a response back
            using (IOutputStream output = socket.OutputStream) {

                using (Stream response = output.AsStreamForWrite()) {

                    // For now we are just going to reply to anything with Hello World!
                    byte[] bodyArray = Encoding.UTF8.GetBytes($"<html><body>{message}</body></html>");

                    var bodyStream = new MemoryStream(bodyArray);

                    // This is a standard HTTP header so the client browser knows the bytes returned are a valid http response
                    var header = "HTTP/1.1 200 OK\r\n" +
                                $"Content-Length: {bodyStream.Length}\r\n" +
                                    "Connection: close\r\n\r\n";

                    byte[] headerArray = Encoding.UTF8.GetBytes(header);

                    // send the header with the body inclded to the client
                    await response.WriteAsync(headerArray, 0, headerArray.Length);
                    await bodyStream.CopyToAsync(response);
                    await response.FlushAsync();
                }
            }
        }

        #endregion



        #region READ

        /// <summary>
        /// Utilizza la classe DataReader che semplifica la lettura di dati complessi
        /// </summary>
        /// <param name="socket"></param>
        /// <returns></returns>
        public async Task<string> ReadWithDataReader( StreamSocket socket ) {
            DataReader reader;

            using (reader = new DataReader(socket.InputStream)) {

                // Set the DataReader to only wait for available data (so that we don't have to know the data size)
                reader.InputStreamOptions = InputStreamOptions.Partial;
                // The encoding and byte order need to match the settings of the writer we previously used.
                reader.UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding.Utf8;
                reader.ByteOrder = ByteOrder.LittleEndian;

                // aspetto l'input dal client
                await reader.LoadAsync(256);
                // leggo una string
                var str = reader.ReadString(reader.UnconsumedBufferLength);

                // chiudo tutto
                reader.DetachStream();
                return str;
            }
        }
                
        /// <summary>
        /// Legge tutto quello che ha inviato il client fino a chiusura connessione
        /// </summary>
        /// <param name="socket"></param>
        /// <returns></returns>
        public string ReadWithStreamReader ( StreamSocket socket ) {
            Stream streamIn = socket.InputStream.AsStreamForRead();
            StreamReader reader = new StreamReader(streamIn);
            return reader.ReadToEnd();
        } 

        /// <summary>
        /// Legge una quantità di dati inferiore al buffer impostato e quindi continua
        /// </summary>
        /// <param name="socket"></param>
        /// <returns></returns>
        public async Task<string> ReadWithBuffer ( StreamSocket socket ) {
            StringBuilder request = new StringBuilder();

            using (IInputStream input = socket.InputStream) {
                byte[] data = new byte[BufferSize];
                IBuffer buffer = data.AsBuffer();
                uint dataRead = BufferSize;
                while (dataRead==BufferSize) {
                    await input.ReadAsync(buffer, BufferSize, InputStreamOptions.Partial).AsTask();
                    request.Append(Encoding.UTF8.GetString(data, 0, (int)buffer.Length));
                    dataRead = buffer.Length;
                }
            }

            return request.ToString();
        }

        #endregion

    }
}
